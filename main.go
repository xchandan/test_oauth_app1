package main

import (
	"context"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/bitbucket"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const htmlIndex = `<html><body>
<a href="/bitbucketLogin">Log in with Bitbucket</a>
</body></html>`

var (
	oauthConfig = &oauth2.Config{
		RedirectURL:  "http://192.168.56.102:4000/bitbucketCallback",
		ClientID:     os.Getenv("bitbucketkey"),
		ClientSecret: os.Getenv("bitbucketsecret"),
		Scopes:       []string{"snippet"},
		Endpoint:     bitbucket.Endpoint,
	}
	// Some random string, random for each request
	oauthStateString = "random"
	accessToken      string
	token            *oauth2.Token
	ctx              = context.Background()
)

func handleMain(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, htmlIndex)
}

func handleBitbucketLogin(w http.ResponseWriter, r *http.Request) {
	//url := oauthConfig.AuthCodeURL(oauthStateString, oauth2.AccessTypeOffline)
	url := oauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func handleBitbucketCallback(w http.ResponseWriter, r *http.Request) {
	var err error
	state := r.FormValue("state")
	if state != oauthStateString {
		fmt.Printf("invalid oauth state, expected '%s', got '%s'\n", oauthStateString, state)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	httpClient := &http.Client{Timeout: 30 * time.Second}
	ctx = context.WithValue(ctx, oauth2.HTTPClient, httpClient)

	code := r.FormValue("code")
	fmt.Printf("Code: %s\n", code)
	token, err = oauthConfig.Exchange(ctx, code)
	if err != nil {
		fmt.Println("Code exchange failed with '%s'\n", err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	fmt.Println(token)
	viewUrl := "/bitbucketview"
	http.Redirect(w, r, viewUrl, http.StatusTemporaryRedirect)
}

func handleBitbucketView(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Access: %s\n", token.AccessToken)
	fmt.Printf("Refress: %s\n", token.RefreshToken)
	oauthClient := oauthConfig.Client(ctx, token)
	response, _ := oauthClient.Get("https://bitbucket.org/!api/2.0/snippets/xchandan/yeGKx4/de38ce74143aa9bf7527e70154f2580caf7526e6/files/myvm.go?access_token=" + token.AccessToken)

	defer response.Body.Close()
	contents, _ := ioutil.ReadAll(response.Body)
	fmt.Fprintf(w, "%s\n", contents)
}

func main() {
	fmt.Println("Starting server ...")
	http.HandleFunc("/", handleMain)
	http.HandleFunc("/bitbucketLogin", handleBitbucketLogin)
	http.HandleFunc("/bitbucketCallback", handleBitbucketCallback)
	http.HandleFunc("/bitbucketview", handleBitbucketView)
	fmt.Println(http.ListenAndServe(":4000", nil))
}
